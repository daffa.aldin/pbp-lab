from webbrowser import get
from django.urls import path
from .views import index, get_watchlist

urlpatterns = [
    path('', index, name='index'),
    path('watch-list/<id>', get_watchlist)
]