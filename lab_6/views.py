from django.shortcuts import render
from lab_5.models import MyWatchlist
from django.http.response import HttpResponse
from django.core import serializers

def index(request):
    watch = MyWatchlist.objects.all()
    response = {'index': watch}
    return render(request, 'lab6_index.html', response)

def get_watchlist(request, id):
    watch = MyWatchlist.objects.get(id = id)
    data = serializers.serialize('json', watch)
    return HttpResponse(data, content_type="application/json")

def update_movie():
    return

def delete_movie():
    return