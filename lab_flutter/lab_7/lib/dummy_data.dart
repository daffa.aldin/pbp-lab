import 'package:flutter/material.dart';

import './models/category.dart';
import './models/meal.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c1',
    title: 'Naruto',
    color: Colors.red,
  ),
  Category(
    id: 'c2',
    title: 'One Piece',
    color: Colors.blue,
  ),
  Category(
    id: 'c3',
    title: 'Demon Slayer',
    color: Colors.green,
  ),
  Category(
    id: 'c4',
    title: 'My Hero Academia',
    color: Colors.orange,
  ),
  Category(
    id: 'c5',
    title: 'Jujutsu Kaisen',
    color: Colors.purple,
  ),
];

const DUMMY_MEALS = const [
  Meal(
    id: 'm1',
    categories: [
      'c1',
    ],
    title: 'The LAST : Naruto The Movie',
    affordability: Affordability.Affordable,
    complexity: Complexity.Simple,
    imageUrl:
        'https://m.media-amazon.com/images/M/MV5BNDQxNzhlMzktMGExMS00OWEzLWIzZTgtMGQ5MmY3ZWU3NDM4XkEyXkFqcGdeQXVyNDgyODgxNjE@._V1_.jpg',
    duration: 20,
    ingredients: [
      'Watched: True',
      'Rating: 10',
      'Release Date: December 6, 2014',
      'Review: FILM TERDEBES',
    ],
    steps: [
      'Naruto Uzumaki: Junko Takeuchi',
      'Sasuke Uchiha: Noriaki Sugiyama',
      'Sakura Haruno: Chie Nakamura',
      'Kakashi Hatake: Kazuhiko Inoue',
      'Hinata Hyuga: Hana Mizuki',
      'Kurama: Tessho Genda',
      'Toneri Otsutsuki: Jun Fukuyama'
    ],
    isGlutenFree: false,
    isVegan: true,
    isVegetarian: true,
    isLactoseFree: true,
  ),
  Meal(
    id: 'm2',
    categories: [
      'c2',
    ],
    title: 'One Piece : STAMPEDE',
    affordability: Affordability.Affordable,
    complexity: Complexity.Simple,
    imageUrl:
        'https://m.media-amazon.com/images/M/MV5BYThmOTJkOTItODRlZC00NzYyLWE1OTItYzAyOTc0ZmI0YjNkXkEyXkFqcGdeQXVyNzI1NzMxNzM@._V1_.jpg',
    duration: 10,
    ingredients: [
      'Watched: True',
      'Rating: 10',
      'Release Date: September 18, 2019',
      'Review: BAGUS'
    ],
    steps: [
      'Monkey D. Luffy: Mayumi Tanaka',
      'Roronoa Zoro: Kazuya Nakai',
      'Boa Hancock: Kotono Mitsuishi',
      'Trafalgar D. Water Law: Hiroshi Kamiya',
      'Douglas Bullet: Tsutomu Isobe'
    ],
    isGlutenFree: false,
    isVegan: false,
    isVegetarian: false,
    isLactoseFree: false,
  ),
  Meal(
    id: 'm3',
    categories: [
      'c2',
    ],
    title: 'One Piece Film : RED',
    affordability: Affordability.Affordable,
    complexity: Complexity.Simple,
    imageUrl:
        'https://m.media-amazon.com/images/M/MV5BYWQwM2QwMjktOTM0My00MWY3LWE5MDQtYmNhMWJjMjI0OGIwXkEyXkFqcGdeQXVyMjA0NzcwMjI@._V1_.jpg',
    duration: 10,
    ingredients: [
      'Watched: True',
      'Rating: 10',
      'Release Date: August 6, 2022',
      'Review: SHANKUSU'
    ],
    steps: [
      'Monkey D. Luffy: Mayumi Tanaka',
      'Roronoa Zoro: Kazuya Nakai',
      'Nami: Akemi Okamura',
      'Trafalgar D. Water Law: Hiroshi Kamiya',
      'Shanks: Shuichi Ikeda'
    ],
    isGlutenFree: false,
    isVegan: false,
    isVegetarian: false,
    isLactoseFree: false,
  ),
  Meal(
    id: 'm4',
    categories: [
      'c3',
    ],
    title: 'Demon Slayer : Mugen Train',
    affordability: Affordability.Pricey,
    complexity: Complexity.Simple,
    imageUrl:
        'https://i0.wp.com/anitrendz.net/news/wp-content/uploads/2021/09/demonslayermugentraintvversionvisual-1-e1632582408736.jpg?resize=696%2C391&ssl=1',
    duration: 45,
    ingredients: [
      'Watched: True',
      'Rating: 9',
      'Release Date: January 6, 2021',
      'Review: RENGOKU-SANNN!'
    ],
    steps: [
      'Tanjiro Kamado: Natsuki Hanae',
      'Nezuko Kamado: Akari Kito',
      'Zenitsu Agatsuma: Hiro Shimono',
      'Inosuke Hashibira: Yoshitsugu Matsuoka',
      'Kyojuro Rengoku: Satoshi Hino',
      'Akaza: Akira Ishida'
    ],
    isGlutenFree: false,
    isVegan: false,
    isVegetarian: false,
    isLactoseFree: true,
  ),
  Meal(
    id: 'm5',
    categories: [
      'c4',
    ],
    title: 'My Hero Academia : World Heroes Mission',
    affordability: Affordability.Luxurious,
    complexity: Complexity.Challenging,
    imageUrl:
        'https://m.media-amazon.com/images/M/MV5BYWZmMWZiZDQtM2YwYS00MzZkLWIyMGUtZGUyNzJkNTc4NTkxXkEyXkFqcGdeQXVyODUyNjMyOTE@._V1_FMjpg_UX1000_.jpg',
    duration: 60,
    ingredients: [
      'Watched: False',
      'Rating: 9',
      'Release Date: November 24, 2021',
      'Review: GOOD'
    ],
    steps: [
      'Izuku Midoriya: Daiki Yamashita',
      'Shoto Todoroki: Yuki Kaji',
      'Katsuki Bakugo: Nobuhiko Okamoto',
      'Ochako Uraraka: Ayane Sakura',
      'Flect Turn: Kazuya Nakai'
    ],
    isGlutenFree: false,
    isVegan: false,
    isVegetarian: false,
    isLactoseFree: false,
  ),
  Meal(
    id: 'm6',
    categories: [
      'c5',
    ],
    title: 'Jujutsu Kaisen Movie : 0',
    affordability: Affordability.Luxurious,
    complexity: Complexity.Challenging,
    imageUrl:
        'https://m.media-amazon.com/images/M/MV5BNWUzOTY4YjItYmRlOS00MTViLTkwOTQtNzJhOWRkNjIyMWExXkEyXkFqcGdeQXVyMTAxMzgzNzYz._V1_.jpg',
    duration: 60,
    ingredients: [
      'Watched: False',
      'Rating: 9',
      'Release Date: March 13, 2022',
      'Review: GOJOOO!!!'
    ],
    steps: [
      'Yuta Okkotsu: Megumi Ogata',
      'Rika: Kana Hanazawa',
      'Satoru Gojo: Yuichi Nakamura',
      'Toge Inumaki: Koki Uchiyama',
      'Maki Zenin: Mikato Komatsu',
      'Suguru Geto: Takahiro Sakurai'
    ],
    isGlutenFree: false,
    isVegan: false,
    isVegetarian: false,
    isLactoseFree: false,
  ),
];
