import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:template_flutter/api_model/post_model.dart';

Future<List<Post>> fetchPost() async {
  String url = "http://127.0.0.1:8000/lab-3/json/";
  http.Response response = await http.get(Uri.parse(url));
  var data = jsonDecode(utf8.decode(response.bodyBytes));
  // print(data);
  List<Post> post = [];
  for (var d in data) {
    if (d != null) {
      post.add(Post.fromMap(d));
    }
  }
  return post;
}

Future api() async {
    const String url = 'https://jsonplaceholder.typicode.com/posts';
    http.Response response = await http.get(Uri.parse(url),
    headers: {
          "Accept": "application/json",
          "Access-Control_Allow_Origin": "*"
        });  // here i passed http.get
    print(response.body); // You should get your result
  }

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Future<List<Post>> futurePost;

  @override
  void initState() {
    super.initState();
    futurePost = fetchPost();
    api();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Latihan Fetch Data',
      theme: ThemeData(
        primaryColor: Colors.lightBlueAccent,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Latihan Fetch Data'),
        ),
        body: FutureBuilder<List<Post>>(
          future: futurePost,
          builder: (context, snapshot) {
            // print(snapshot);
            if (snapshot.hasData) {
              return ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                physics: const ClampingScrollPhysics(),
                itemCount: snapshot.data!.length,
                itemBuilder: (_, index) => Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  padding: const EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 179, 172, 41),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        snapshot.data![index].fields.title,
                        style: const TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 10),
                      Text(snapshot.data![index].fields.releaseDate),
                      Text(snapshot.data![index].fields.review),
                    ],
                  ),
                ),
              );
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }

  
}