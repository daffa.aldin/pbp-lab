from django import forms
from lab_2.models import TrackerTugas

class TugasForm(forms.ModelForm):
    class Meta:
        model = TrackerTugas
        fields = "__all__"
        widgets = {
            'course': forms.TextInput(attrs={"placeholder":"Masukkan Course"}),
            'detail': forms.Textarea(attrs={"rows": 3}),
            'deadline': forms.DateInput()
        }