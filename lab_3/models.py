from django.db import models

class MyWatchlist(models.Model):
    watched = models.BooleanField(default=False)
    title = models.CharField(max_length=255)
    rating = models.FloatField(default=0.0)
    release_date = models.TextField()
    review = models.TextField()