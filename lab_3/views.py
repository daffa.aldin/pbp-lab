from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from lab_3.models import MyWatchlist

def index(request):
    watch = MyWatchlist.objects.all()
    response = {'index': watch}
    return render(request, 'lab3.html', response)

def xml(request):
    watch = MyWatchlist.objects.all()
    data = serializers.serialize('xml', watch)
    return HttpResponse(data, content_type="application/xml")

def json(request):
    watch = MyWatchlist.objects.all()
    data = serializers.serialize('json', watch)
    return HttpResponse(data, content_type="application/json")