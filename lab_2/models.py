from django.db import models
from datetime import date, datetime

class TrackerTugas(models.Model):
    course = models.CharField(max_length=30)
    detail = models.CharField(max_length=255, default="Something")
    deadline = models.DateTimeField(default=datetime.now())