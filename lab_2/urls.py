from django.urls import path
from .views import index, list_tugas

urlpatterns = [
    path('', index, name='index'),
    path('list-tugas/', list_tugas)
]
