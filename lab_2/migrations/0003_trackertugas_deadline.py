# Generated by Django 3.2.7 on 2022-02-24 04:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0002_trackertugas_detail'),
    ]

    operations = [
        migrations.AddField(
            model_name='trackertugas',
            name='deadline',
            field=models.CharField(default='Something', max_length=255),
        ),
    ]
