from django import forms
from .models import MyWatchlist

class WatchlistForm(forms.ModelForm):
    class Meta:
        model = MyWatchlist
        fields = "__all__"
        widgets = {
            'watched': forms.CheckboxInput(),
            'title': forms.TextInput(attrs={"placeholder":"Masukkan Title"}),
            'rating': forms.NumberInput(),
            'release_date': forms.DateInput(),
            'review': forms.Textarea(attrs={"rows": 3}),
        }