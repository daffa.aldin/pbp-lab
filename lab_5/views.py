from django.shortcuts import render
from .models import MyWatchlist
from .forms import WatchlistForm
from django.contrib.auth.decorators import login_required

def index(request):
    watch = MyWatchlist.objects.all()
    response = {'index': watch}
    return render(request, 'lab5_index.html', response)

def watch_list(request):
    watch = MyWatchlist.objects.all()
    response = {'watch_list': watch}
    return render(request, 'lab5_watch_list.html', response)

@login_required(login_url='/admin/login/')
def add_watchlist(request):
    form = WatchlistForm(request.POST , request.FILES)
    if request.method == "POST":
        if form.is_valid():
            form.save()
    response = {'form': form}
    return render(request, "lab5_form.html", response)