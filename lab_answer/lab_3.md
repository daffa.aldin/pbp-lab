1. Apakah perbedaan antara JSON dan XML?
    a. JSON
        - Membungkus informasi menggunakan kombinasi key dan value yang self descriptive
        - Sintaks pada JSON merupakan turunan dari Object JavaScript. Akan tetapi, format pada JSON berbentuk text, sehingga kode untuk membaca dan membuat JSON juga terdapat di banyak bahasa pemrograman

    b. XML
        - Membungkus informasi menggunakan tag yang self descriptive
        - Dokumen XML membentuk struktur layaknya tree yang dimulai dari root, kemudian branch, hingga berakhir pada leaves
        - Di dalam Dokumen XML, harus terdapat sebuah root element yang merupakan parent dari element lainnya

2. Apakah perbedaan antara HTML dan XML?
    a. HTML
        - Membungkus informasi menggunakan tag yang tidak self descriptive, harus mengikuti aturan baku penamaan tag
        - Mendeskripsikan struktur dari sebuah halaman web. Di dalam HTML, terdapat beberapa macam elemen yang akan menyampaikan informasi kepada browser bagaimana untuk menampilkan konten
        - Elemen pada HTML didefinisikan dengan tag pembuka (`start tag`), beberapa konten, dan tag penutup (`end tag`)

    b. XML
        - Membungkus informasi menggunakan tag yang self descriptive, pengguna dapat membuat tag yang sesuai dengan pemahamannya
        - Dokumen XML membentuk struktur layaknya tree yang dimulai dari root, kemudian branch, hingga berakhir pada leaves
        - Di dalam Dokumen XML, harus terdapat sebuah root element yang merupakan parent dari element lainnya